// applicationBoite.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <string>
#include <iostream>
#include <map>

using namespace std;

struct IdeeLike
{
	string idee = {};
	int Liker = 0;
	int Disliker = 0;
};

class application
{
public:
	application(){};
	~application() {};
	void lancer() {
		string adresseEmail = {};
		bool isAdresseValide = false;
		cout << "Entrer votre adresse �mail pour vous connecter :";
		getline(std::cin, adresseEmail);
		do {
				isAdresseValide = checkAdress(adresseEmail);
				if (isAdresseValide == false)
				{
					cout << "Adresse non valide , veuillez rentrer une adresse �mail valide : ";
					getline(std::cin, adresseEmail);
				}
		} while (isAdresseValide == false);

		AfficherLesOptions();
	}

	bool checkAdress(const string adresseEmail) {
		bool adressValide = false;
		auto it = adresseEmail.find("@");
		if (it != std::string::npos)
		{
			it = adresseEmail.find(".");
			if (it != std::string::npos)
			{
				adressValide = true;

			}
		}
		return adressValide;
	}
	//Les options de saisis
	void AfficherLesOptions() {
		string choixOption = {};
		int numeroCle = 0;
		cout << "Choix d'options pour manipuler les idees, tapez une lettre : " << endl;
		cout << "Poster: P; ";
		if (m_map_idees.size())
		{
			cout << " modifier: M; supprimer : S; liker/Disliker: L; ";
		}
		cout << "Quitter :E" << endl;
		getline(std::cin, choixOption);
		TraitementChoix(choixOption);
		
	}
	void TraitementChoix(string &choixOption)
	{
		char choix  =' ';
		string nouvelleIdee = {};
		if (choixOption.size() >= 1)
		{
			choixOption = choixOption.substr(0, 1);
			choix = toupper(choixOption[0]);
		}

		if (choix == 'P') {
			cout << "Veuillez entrer votre idee sur une seule ligne et valider par la touche entree : " << endl;
			getline(std::cin, nouvelleIdee);
			Poster_idee(nouvelleIdee);
			AfficherIdees();
			AfficherLesOptions();
		}
		else if (choix == 'M' && m_map_idees.size()) {
			cout << "Veuillez un Id de l'idee que vous voulez changez " << endl;
			std::map<uint64_t, IdeeLike>::iterator it = SaisirId();
			//if (IdIdee >= 0)
			//
				cout << "Veuillez rentrer la modification apportee " << endl;
				getline(std::cin, nouvelleIdee);
				modifier_idee(it, nouvelleIdee);
			//}
			AfficherIdees();
		}
		else if (choix == 'S' && m_map_idees.size()) {
			cout << "Veuillez entrer l'Id de l'idee que vous voulez supprimer " << endl;
			std::map<uint64_t, IdeeLike>::iterator it = SaisirId();
			supprimer_idee(it);
			AfficherIdees();
		}
		else if (choix == 'L' && m_map_idees.size()) {
			cout << "Veuillez entrer l'Id de l'idee que vous voulez commenter " << endl;
			std::map<uint64_t, IdeeLike>::iterator it = SaisirId();
			cout << "Veuillez entrer une valeur Like : 1; Dislike 0 " << endl;
			TraiterLikeDislike(it);
			AfficherIdees();
		}
		else if (choix == 'E')
		{
			exit(0);
		}else
		{
			cout << "Veuillez entrer un choix valide " << endl;
			AfficherLesOptions();
		}
	}
	//traitement like dislike
	void TraiterLikeDislike(map<uint64_t, IdeeLike>::iterator it)
	{
		int Like = 0;
		while (!(std::cin >> Like) || (Like != 0 && Like != 1))
		{
			std::cout << "Saisie incorrecte, recommencez : ";
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		if (Like == 0)
		{
			it->second.Disliker = it->second.Disliker++;
		}
		else
		{
			it->second.Liker = it->second.Liker++;
		}
	}
	std::map<uint64_t, IdeeLike>::iterator SaisirId()
	{
		uint64_t IdIdee = -1;
		while (!(std::cin >> IdIdee) || IdIdee <0  || m_map_idees.find(IdIdee) == m_map_idees.end())
		{
			std::cout << "Saisie incorrecte, recommencez : ";
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		return m_map_idees.find(IdIdee);
	}

	//affichage des id�es
	void AfficherIdees() {
		map<uint64_t, IdeeLike>::iterator pos;
		if (m_map_idees.size() > 0)
		{
			cout << "la liste des idees contient : " << endl;
		}
		else {
			cout << "la liste des idees est vide : " << endl;
		}

		for (pos = m_map_idees.begin(); pos != m_map_idees.end(); pos++)
		{
			string idee = pos->second.idee;
			cout << "l'idee Id : " << pos->first << " Text : " << idee <<endl;
			cout << "nombre de like: " << pos->second.Liker << "; " << "nombre dislike: " << pos->second.Disliker <<endl<< endl;
		}

		AfficherLesOptions();
	}
	//**ajouter une id�e
	void Poster_idee(const string & idee)
	{
		IdeeLike nouvelleIdee;
		nouvelleIdee.idee = idee;
		uint64_t cle = m_map_idees.size();
		m_map_idees.insert(pair<uint64_t, IdeeLike>(cle, nouvelleIdee));
	}

	//suppression d'une id�e
	void supprimer_idee(map<uint64_t, IdeeLike>::iterator it)//uint64_t numeroIdee)
	{
		m_map_idees.erase(it->first);
	}

	void modifier_idee(map<uint64_t, IdeeLike>::iterator it, const string & nouvelleIdee)
	{
		it->second.idee = nouvelleIdee;	
	}


private:
	map<uint64_t, IdeeLike> m_map_idees{ std::map<uint64_t, IdeeLike> {} };
};

int main()
{
	application* myApp = new application();
	myApp->lancer();
	return 0;
}

/* ; *On souhaite avoir une application boite � idees(Une idee peut �tre uniquement du texte), et lamettre � disposition de nos employ�s, et chacun d'eux doit pouvoir:

- Se connecter en utilisant uniquement son adresse e - mail.

- Visualiser les idees post�s par tous les autre employ�s.

- "Liker" ou "Disliker" une idee.

- Poster une nouvelles idees.

- Modifier une idee qu'il a d�j� post�.

- Supprimer une idee qu'il a d�j� post�.*/